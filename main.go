package main

import (
	"expvar"
	"flag"
	"html/template"
	"log"
	"net/http"
	"sync"
	"time"
	"fmt"
	"io"
	"strings"
	"net/url"
)

// Command-line flags.
var (
	httpAddr   = flag.String("http", ":8080", "Listen address")
	pollPeriod = flag.Duration("poll", 1*time.Second, "Poll period")
	website    = flag.String("website", "https://app.annea.ai", "Website URL")
)

const baseURL = "https://api.websitecarbon.com/site?url="

func main() {
	flag.Parse()
	changeURL := fmt.Sprintf("%s%s", baseURL, url.QueryEscape(*website)) 
	http.Handle("/", NewServer(*website, changeURL, *pollPeriod))
	log.Printf("serving on http://localhost%s and http://0.0.0.0%s", *httpAddr, *httpAddr)
	log.Fatal(http.ListenAndServe(*httpAddr, nil))
}

// Exported variables for monitoring the server.
// These are exported via HTTP as a JSON object at /debug/vars.
var (
	hitCount       = expvar.NewInt("hitCount")
	pollCount      = expvar.NewInt("pollCount")
	pollError      = expvar.NewString("pollError")
	pollErrorCount = expvar.NewInt("pollErrorCount")
	lastPollDuration = expvar.NewInt("lastPollDuration")
)

// Server serves the user interface (it's an http.Handler)
type Server struct {
	version string
	url     string
	period  time.Duration

	mu  sync.RWMutex // protects the green variable
	Green bool
}

// NewServer returns an initialized server.
func NewServer(version, url string, period time.Duration) *Server {
	s := &Server{version: version, url: url, period: period}
	go s.poll()
	return s
}

func (s *Server) poll() {
	log.Print("Polling")
	for true {
		s.Green = checkCo2(s.url)
		pollSleep(s.period)
	}
	s.mu.Lock()
	s.mu.Unlock()
	pollDone()
}

var (
	pollSleep = time.Sleep
	pollDone  = func() {}
)

// checkCo2 makes an HTTP GET request to the Website Carbon API and tells 
// whether the website is considered green or not
func checkCo2(url string) bool {
	pollCount.Add(1)
	//urlWithCacheInvalidation := fmt.Sprintf("%s?%s", url, pollCount)

	start := time.Now()
	r, err := http.Get(url)
	lastPollDuration.Set(time.Since(start).Milliseconds())
	log.Print("lastPollDuration in milliseconds: ")
	log.Print(lastPollDuration)

	if err != nil {
		log.Print(err)
		pollError.Set(err.Error())
		pollErrorCount.Add(1)
		return false
	}

	body, err := io.ReadAll(r.Body)
	r.Body.Close()
	if r.StatusCode > 299 {
		log.Fatalf("Response failed with status code: %d and\nbody: %s\n", r.StatusCode, body)
	}
	if err != nil {
		log.Fatal(err)
	}
	// Debug output 
	// fmt.Printf("%s", body)

	return strings.Contains(string(body), "true")
}

// ServeHTTP implements the HTTP user interface.
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	hitCount.Add(1)
	s.mu.RLock()
	data := struct {
		URL     string
		Version string
		Green   bool
	}{
		s.url,
		s.version,
		s.Green,
	}
	s.mu.RUnlock()
	err := tmpl.Execute(w, data)
	if err != nil {
		log.Print(err)
	}
}

// tmpl is the HTML template that drives the user interface.
var tmpl = template.Must(template.New("tmpl").Parse(`
<!DOCTYPE html><html><body><center>
	<h2>Is the site considered green by https://www.websitecarbon.com/how-does-it-work/ ?</h2>
	<h1>
	{{if .Green}}
		YES!
	{{else}}
		No. :-(
	{{end}}
	</h1>
</center></body></html>
`))
