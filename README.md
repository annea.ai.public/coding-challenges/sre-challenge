# sre-challenge

This repo contains a small challenge to test your SRE skills ;). 

Situation: we were concerned about if our platform is environment friendly or not. To answer this we found the [Website Carbon API](https://api.websitecarbon.com/).

We built a small Go application that regularly makes requests to the API and checks if the site app.annea.ai is considered "green" or not.  

**Pre-requisite for the challenge:**
- Docker
- Docker Compose

**How to store the changes made to answer the challenge:**
1. Initialise a new git repository with these files
2. Split changes into several commits with comprehensive messages
3. Create a `.tar` archive that contains the changes + your `.git` folder
4. Send the archive back to the person who sent you the challenge

*You can create a `NOTES.md` file to add comments to your answer*

## First mission: figure out if app.annea.ai is considered green or not

```
docker compose up --build
```
Open http://localhost:8080, you should see
```
Is the site considered green by https://www.websitecarbon.com/how-does-it-work/ ? YES!
```

Additionally to the main page, we exported some metrics about the application here http://localhost:8080/debug/vars .

## Second mission: checkout Prometheus error

Open http://localhost:9090/targets . Observe that the target "website-carbon-poller ( http://localhost:8080/debug/vars )" is throwing an erro "INVALID is not a valid start token".

## Third mission: figure out the error and fix it

Figure out why Prometheus can't read these metrics in JSON format. Then, implement a solution so that Prometheus can read the metrics of the app.

## Fourth mission: create a Grafana dashboard to display metrics

Now that Prometheus can read the metrics, create a Grafana Dashboard that will read some of the metrics. The dashboard should at least show a graph of "lastPollDuration" values. The dashboard should help to monitor the app.

Don't forget to save the created dashboard so that you can send it to us part of your answer to the challenge.

## Fifth mission: optimise the Dockerfile

The Dockerfile we made for the app is not optimised in terms of average build time. Find out the best optimisation and implement it.

## [Bonus] Improve the overall setup

If you accomplished all the mandatory missions and are enjoying the challenge, then you can be creative and do additional things that will be taken in account for the assessement. Here are some ideas:
- Export a new metric from the Go application, and add it to the dashboard
- Create alerts
- Create a Kubernetes manifest to deploy the Go app
- ...
