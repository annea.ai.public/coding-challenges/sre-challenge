FROM golang:1.18.0

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy all source code
COPY . .

# Download dependencies
RUN go mod download

# Build the application
RUN go build -o main .

EXPOSE 8080

# Command to run the executable
CMD ["./main"]
